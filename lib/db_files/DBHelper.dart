import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:phonebook/model/Contact.dart';
import 'package:phonebook/model/Farmer.dart';
import 'package:sqflite/sqflite.dart';

class DBHelper {
  String contact_table = "contacts";
  String id = "id";
  String surname = "surname";

  String firstname = "firstname";
  String lastname = "lastname";
  String gender = "gender";
  String farmer_image = "farmer_image";

  String img = "image";
  String other_names = "othernames";
  String uuid = "uuid";
  String phone_number = "phonenumber";
  String location = "location";
  String status = "status";
  static Database _database;
  static DBHelper _dbHelper;

  DBHelper._createInstance();

  factory DBHelper() {
    if (_dbHelper == null) {
      _dbHelper = DBHelper._createInstance();
    }
    return _dbHelper;
  }

  Future<Database> initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = documentsDirectory.path + "phonebook.db";

    var phonebookDb = await openDatabase(path, version: 1, onCreate: _createDb);
    return phonebookDb;
  }

  Future<Database> get database async {
    if (_database == null) {
      _database = await initDB();
    }
    return _database;
  }

  void _createDb(Database db, int version) async {
    await db.execute(
        'CREATE TABLE $contact_table(id INTEGER PRIMARY KEY AUTOINCREMENT,$uuid TEXT,$surname TEXT,$other_names TEXT,$img BLOB,$location TEXT,$phone_number TEXT,$status TINYINT);');
  }

  getContactkMapList() async {
    Database db = await this.database;

    var result = await db.query(contact_table);
//    var result = await db.rawQuery(
//        'SELECT id,surname,othernames,phonenumber,location,image FROM $contact_table');

    return result;
  }

  getUnsyncedContactkMapList() async {
    Database db = await this.database;

    var result = await db.rawQuery(
        'SELECT surname,othernames,uuid,phonenumber,location,image FROM $contact_table WHERE status = 0',);
//    var result =
//        await db.query(contact_table, where: 'status = ?', whereArgs: [0]);
    print("Unsynced $result");
    print("Unsynced JSON Encode\n${json.encode(result)}\n");
   // print("Unsynced JSON Decode\n${json.decode(result.toString())}\n");
    print("Unsynced String ${result.toString()}");
    return result;
  }

  Future<int> insertContact(Contact contact) async {
    Database db = await this.database;
   var result = await db.insert(contact_table, contact.toMap());
  //  var result = await db.rawInsert("INSERT INTO contacts (surname, othernames, location, phonenumber, status,image) VALUES (${contact.surname}, ${contact.other_names}, ${contact.location}, ${contact.phone_number},${contact.status}, ${contact.image})");
//    var res = await db.rawInsert(
//        "INSERT Into Client (id,first_name)"
//            " VALUES (${newClient.id},${newClient.firstName})");
    return result;
  }

  Future<int> insertFarmer(Farmer farmer) async {
    Database db = await this.database;
    var result = await db.insert(contact_table, farmer.toMap());
    return result;
  }

  Future<int> updateFarmer(Farmer farmer) async {
    Database db = await this.database;
    var result = await db.update(contact_table, farmer.toMap(),
        where: 'id = ?', whereArgs: [farmer.id]);
    return result;
  }

  Future<int> updateContact(Contact contact) async {
    Database db = await this.database;
    var result = await db.update(contact_table, contact.toMap(),
        where: 'id = ?', whereArgs: [contact.id]);
    return result;
  }

  Future<int> deleteContact(int id) async {
    Database db = await this.database;
    var result =
        await db.rawDelete('DELETE FROM $contact_table WHERE id = $id;');
    return result;
  }

  Future<int> updateUnsyncedContact(dynamic id) async {
    Database db = await this.database;
    var result =
    await db.rawUpdate('UPDATE $contact_table SET status = 1 WHERE id = $id;');
    return result;
  }

  Future<int> getCount() async {
    Database db = await this.database;
    List<Map<String, dynamic>> x =
        await db.rawQuery('SELECT COUNT (*) FROM $contact_table;');
    int result = Sqflite.firstIntValue(x);
    return result;
  }

  Future<List<Contact>> getContactList() async {
    var contactMapList = await getContactkMapList();
    int count = contactMapList.length;

    List<Contact> contactList = List<Contact>();

    for (int i = 0; i < count; i++) {
      contactList.add(Contact.fromMapObject(contactMapList[i]));
    }
    return contactList;
  }

  Future<List<Contact>> getUnsyncedContact() async {
    var contactMapList = await getUnsyncedContactkMapList();
    int count = contactMapList.length;

    List<Contact> officerList = List<Contact>();

    for (int i = 0; i < count; i++) {
      officerList.add(Contact.fromMapObject(contactMapList[i]));
    }
    return officerList;
  }
}
