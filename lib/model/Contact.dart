import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class Contact {
  int _id, _status;
  String _surname, _other_names, _phone_number, _location;
  dynamic _image, _uuid;

  Contact.NoId(this._surname, this._other_names, this._phone_number,
      this._location, this._image);

  Contact(this._id, this._surname, this._other_names, this._phone_number,
      this._location, this._image, this._status, this._uuid);

  int get id => _id;

  int get status => _status;

  String get surname => _surname;

  String get other_names => _other_names;

  String get phone_number => _phone_number;

  String get location => _location;

  dynamic get image => _image;

  dynamic get uuid => _uuid;

  set surname(String surname) {
    this._surname = surname;
  }

  set other_names(String other_names) {
    this._other_names = other_names;
  }

  set phone_number(String phone_number) {
    this._phone_number = phone_number;
  }

  set location(String location) {
    this._location = location;
  }

  set image(dynamic image) {
    this._image = image;
  }

  set uuid(dynamic uuid) {
    this._uuid = uuid;
  }

  set status(int status) {
    this._status = status;
  }

  //Convert object to Map
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    if (id != null) {
      map['id'] = _id;
    }
    map['surname'] = _surname;
    map['uuid'] = _uuid;
    map['othernames'] = _other_names;
    map['location'] = _location;
    map['status'] = _status;
    map['phonenumber'] = _phone_number;
    map['image'] = _image;

    return map;
  }

  Map<String, dynamic> toJson() => {
        //"id": _id,
        "surname": _surname,
        "othernames": _other_names,
        "location": _location,
        "phonenumber": _phone_number,
        "uuid": _uuid,
        //  "status": _status,
        "image": _image
      };

  //extract note obj from map
  Contact.fromMapObject(Map<dynamic, dynamic> map) {
    _id = map['id'];
    _uuid = map['uuid'];
    _surname = map['surname'];
    _other_names = map['othernames'];
    _location = map['location'];
    _phone_number = map['phonenumber'];
    _image = map['image'];
    _status = map['status'];
  }
}
