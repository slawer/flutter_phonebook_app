import 'dart:convert';

class Farmer {
  String billing_info_id;
  String amount;
  String network;
  String trans_type;
  String customer_number;
  String appointment_title;

  int id;
  String surname, other_names, phone_number, location;
  dynamic image;

  Farmer({
//    this.id,
    this.surname,
    this.other_names,
    this.phone_number,
    this.location,
    this.image,
  });

  factory Farmer.fromJson(Map<String, dynamic> json) {
    return Farmer(
//        id: json['id'],
        surname: json['surname'],
        other_names: json['other_names'],
        phone_number: json['phone_number'],
        location: json['location'],
        image: json['image']);
  }

  Map<String, dynamic> toJson() => {
//        "id": id,
        "surname": surname,
        "other_names": other_names,
        "phone_number": phone_number,
        "location": location,
        "image": image
      };

  Farmer postFromJson(String str) {
    final jsonData = json.decode(str);
    return Farmer.fromJson(jsonData);
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    if (id != null) {
      map['id'] = id;
    }
    map['surname'] = surname;
    map['other_names'] = other_names;
    map['location'] = location;
    map['phone_number'] = phone_number;
    map['image'] = image;

    return map;
  }

  String postToJson(Farmer data) {
    final dyn = data.toJson();
    return json.encode(dyn);
  }
}
