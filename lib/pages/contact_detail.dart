import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:phonebook/db_files/DBHelper.dart';
import 'package:phonebook/model/Contact.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart';
import 'package:image_picker/image_picker.dart';
import 'package:phonebook/model/Farmer.dart';
import 'package:uuid/uuid.dart';

class ContactDetail extends StatefulWidget {
  final String appBarTitle;
  final Contact contact;

  final String title = "Upload Image Demo";

  ContactDetail(this.contact, this.appBarTitle);

  @override
  _ContactDetailState createState() =>
      _ContactDetailState(this.contact, this.appBarTitle);
}

class _ContactDetailState extends State<ContactDetail> {
  DBHelper helper = DBHelper();

  String appBarTitle;
  Contact contact;
  Geolocator geolocator = Geolocator();

  Position userLocation;

  _ContactDetailState(this.contact, this.appBarTitle);

  final surnameController = TextEditingController();
  final otherNamesController = TextEditingController();
  final locationController = TextEditingController();
  final imageController = TextEditingController();
  final phoneNumberController = TextEditingController();

  String _surname, _otherName, _location, _image, _phoneNumber;

  var _formKey = GlobalKey<FormState>();
  var btnText;

  String error;

  String lat;
  String long;

  var currentLocation = LocationData;
  var location = new Location();
  Map<dynamic, dynamic> current = new Map();

  FocusNode _focusNode;
  FocusNode _focus;

  File _imageFile;
  List<int> imageBytes;
  List<int> dBytes;
  String imageB64;
  dynamic imaB64;
  dynamic decoded;
  dynamic deco;

  dynamic jsonContact;

  var uuid = new Uuid();

  @override
  void initState() {
    _focusNode = FocusNode();
    _focus = FocusNode();

    super.initState();

    lat = "0.0";
    long = "0.0";
    // jsonContact = json.encode(this.contact);
    jsonContact = this.contact;

    print("55 ${this.contact.toString()}");
    print("88 ${this.contact.toMap()}");
    print("33 ${jsonContact}");

    //imaB64 = base64Encode(jsonContact['image']);
    deco = jsonContact.image;

    location.onLocationChanged().listen((dynamic currentLocation) {
      print("Lat ${currentLocation.latitude}");
      print("Long ${currentLocation.longitude}");
      lat = "${currentLocation.latitude}";
      long = "${currentLocation.longitude}";
      print("Current ${current}");

      if (mounted) {
        setState(() {
          locationController.text =
              "${currentLocation.latitude},${currentLocation.longitude}";
        });
      }
    });
  }

  void _openImagePicker() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            height: 150,
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Text(
                  'Pick an Image',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10.0,
                ),
                FlatButton(
                  child: Text('Use Camera'),
                  onPressed: () {
                    _getImage(ImageSource.camera);
                  },
                ),
                FlatButton(
                  child: Text('Use Gallery'),
                  onPressed: () {
                    _getImage(ImageSource.gallery);
                  },
                ),
              ],
            ),
          );
        });
  }

  void _getImage(ImageSource source) {
    ImagePicker.pickImage(source: source, maxWidth: 400.0).then((File image) {
      setState(() {
        _imageFile = image;

        imageBytes = _imageFile.readAsBytesSync();
        imageController.text = imageBytes.toString();

        // imageBytes = _imageFile.readAsBytesSync();
        imageB64 = base64Encode(imageBytes);
        decoded = base64Decode(imageB64);
        deco = base64Decode(imageB64);
        updateImage(deco);

        print("ImageBytes $imageBytes\n");
        print("Base64Encode $imageB64\n");
        print("Base64Decode $decoded");
      });

      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.title;
    //var _base64 = base64.encode(contact.image as List<int>);
    //Uint8List bytes = base64.decode(contact.image);

    if (appBarTitle == "Edit") {
      btnText = "Update";
    } else {
      btnText = "Save";
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(appBarTitle + ' Contact'),
      ),
      body: Form(
        key: _formKey,
        //   padding: const EdgeInsets.only(top: 15.0, left: 10.0, right: 10.0),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: ListView(
            children: <Widget>[
//              _imageFile == null
//                  ? Image.asset(
//                      "images/image_placeholder.png",
//                      fit: BoxFit.fill,
//                      height: 300.0,
//                      width: MediaQuery.of(context).size.width,
//                      alignment: Alignment.topCenter,
//                    ) //Text('Please pick an image')
//                  : Image.memory(
//                      decoded,
//                      fit: BoxFit.fill,
//                      height: 300.0,
//                      width: MediaQuery.of(context).size.width,
//                      alignment: Alignment.topCenter,
//                    ),
              deco == null
                  ? Image.asset(
                      "images/image_placeholder.png",
                      fit: BoxFit.fill,
                      height: 300.0,
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.topCenter,
                    )
                  : Image.memory(
                      deco,
                      fit: BoxFit.cover,
                      height: 300.0,
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.topCenter,
                    ),
              OutlineButton(
                onPressed: () {
                  _openImagePicker();
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.camera_alt),
                    SizedBox(
                      width: 5.0,
                    ),
                    Text('Add Image')
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                child: TextFormField(
                  initialValue: contact.surname,
                  style: textStyle,
                  textInputAction: TextInputAction.next,
                  autofocus: true,
                  onFieldSubmitted: (v){
                    FocusScope.of(context).requestFocus(_focusNode);
                  },
//                  focusNode: _focusNode,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Enter surname';
                    }
                  },
                  onSaved: (value) {
                    debugPrint('Something change in Surname');
                    _surname = value;
                    updateSurname(value);
                  },
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.text_rotation_none),
                      labelText: 'Surname',
                      labelStyle: textStyle,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                child: TextFormField(
                  initialValue: contact.other_names,
                  style: textStyle,
                  onFieldSubmitted: (v){
                    FocusScope.of(context).requestFocus(_focus);
                  },
                  focusNode: _focusNode,
                  textInputAction: TextInputAction.next,
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Enter othername(s)';
                    }
                  },
                  onSaved: (value) {
                    debugPrint('Something change in Othename');
                    _otherName = value;
                    updateOthernames(value);
                  },
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.title),
                      labelText: 'Othernames',
                      labelStyle: textStyle,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                child: TextFormField(
                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                  inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                  initialValue: contact.phone_number,
                  textInputAction: TextInputAction.done,
                  style: textStyle,
                  focusNode: _focus,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Enter phone number';
                    }
                  },
                  onSaved: (value) {
                    updatePhoneNumber(value);
                    _phoneNumber = value;
                    debugPrint('Something change in Phone');
                  },
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.phone),
                      labelText: 'Phone Number',
                      labelStyle: textStyle,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                child: TextFormField(
                  style: textStyle,
                  enabled: false,
                  controller: locationController,
                  validator: (String value) {
                    if (value.isEmpty) {
                      Fluttertoast.showToast(
                          msg: 'Kindly enable ypur location.',
                          toastLength: Toast.LENGTH_LONG,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIos: 2,
                          backgroundColor: Colors.red,
                          textColor: Colors.white,
                          fontSize: 16.0);
                    }
                  },
                  onSaved: (value) {
                    updateLocation(value);
                    _location = value;
                    debugPrint('Something change $value Location');
                  },
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.map),
                      labelText: 'Location',
                      labelStyle: textStyle,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: RaisedButton(
                        color: Colors.blue,
                        onPressed: () {
                          setState(() {
                            if (_formKey.currentState.validate()) {
                              _formKey.currentState.save();
                              _save(context);

                              debugPrint('Save Button');
                            } else {
                              return null;
                            }
                          });
                        },
                        child: Text(
                          btnText,
                          textScaleFactor: 1.2,
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void updateSurname(String surname) {
    contact.surname = surname;
  }

  void updateOthernames(String othername) {
    contact.other_names = othername;
  }

  void updateLocation(String location) {
    contact.location = "[$location]";
  }

  void updateImage(dynamic image) {
    if (deco == null || image == null){
      Fluttertoast.showToast(
          msg: 'Upload an image.',
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 2,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }else{
      contact.image = image;
    }

  }

  void updatePhoneNumber(String phonenumber) {
    contact.phone_number = phonenumber;
    contact.status = 0;


  }

  void _save(BuildContext context) async {
    Navigator.pop(context, true);

    int result;
    if (contact.id != null) {
      if (contact.uuid == null){
        contact.uuid = uuid.v1();
      }
      result = await helper.updateContact(contact);
    } else {
      if (contact.uuid == null){
        contact.uuid = uuid.v1();
      }
      result = await helper.insertContact(contact);
    }

    if (result != 0) {
      Fluttertoast.showToast(
          msg: "Contact saved successfully",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 2,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
      //_showAlertDialog('Status', 'Contact saved successfully');
      //_showSnackBar(context,'Contact save successfullt');
    } else {
      Fluttertoast.showToast(
          msg: 'Something happened. Please try again later.',
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 2,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }
  }

  void createFarmer(Farmer farmer) async {
    int result;
    if (farmer.id != null) {
      result = await helper.updateFarmer(farmer);
    } else {
      result = await helper.insertFarmer(farmer);
    }

    if (result != 0) {
      Fluttertoast.showToast(
          msg: "Contact saved successfully",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 2,
          backgroundColor: Colors.green,
          textColor: Colors.white,
          fontSize: 16.0);
      //_showAlertDialog('Status', 'Contact saved successfully');
      //_showSnackBar(context,'Contact save successfullt');
    } else {
      Fluttertoast.showToast(
          msg: 'Something happened. Please try again later.',
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 2,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
    }

    // print('***************${response.request.toString()}');
    //print('***************${response.statusCode.toString()}');
    //print('***************${response.body.toString()}');

    // return response;
  }

//  Future<Position> _getLocation() async {
//    var currentLocation;
//    try {
//      currentLocation = await geolocator.getCurrentPosition(
//          desiredAccuracy: LocationAccuracy.best);
//    } catch (e) {
//      currentLocation = null;
//    }
//    return currentLocation;
//  }

}
