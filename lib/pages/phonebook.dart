import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:phonebook/db_files/DBHelper.dart';
import 'package:phonebook/model/Contact.dart';
import 'package:phonebook/pages/contact_detail.dart';
import 'package:phonebook/utils/APILinks.dart';
import 'package:sqflite/sqflite.dart';
import 'package:connectivity/connectivity.dart';
import 'package:http/http.dart' as http;

class PhoneBook extends StatefulWidget {
  @override
  _PhoneBookState createState() => _PhoneBookState();
}

class _PhoneBookState extends State<PhoneBook> {
  DBHelper _dbHelper = new DBHelper();
  List<Contact> contactList;
  int count = 0;
  var _connectionStatus = "Unknown";
  var connectivity;
  StreamSubscription<ConnectivityResult> subscription;

  @override
  void initState() {
    super.initState();
    connectivity = new Connectivity();
    subscription =
        connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      _connectionStatus = result.toString();
      print(_connectionStatus);

      if (result == ConnectivityResult.wifi ||
          result == ConnectivityResult.mobile) {
        //make request here
        doSyncAction();
        Fluttertoast.showToast(
            msg: "Online",
            backgroundColor: Colors.green,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            toastLength: Toast.LENGTH_LONG);
      } else {
        Fluttertoast.showToast(
            msg: "Check your internet connection",
            backgroundColor: Colors.red,
            gravity: ToastGravity.BOTTOM,
            textColor: Colors.white,
            toastLength: Toast.LENGTH_LONG);
      }
    });
  }

  @override
  void dispose() {
    subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (contactList == null) {
      contactList = List<Contact>();
      updateListView();
      doSyncAction();
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('PhoneBook'),
      ),
      body: getPhoneBookListView(),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          debugPrint("FAB clicked");
          navigateToDetail(Contact.NoId('', '', '', '', null), 'Add');
        },
        tooltip: 'Add Contact',
        child: Icon(Icons.add),
      ),
    );
  }

  ListView getPhoneBookListView() {
    TextStyle style = Theme.of(context).textTheme.subhead;

    return ListView.builder(
        itemCount: count,
        itemBuilder: (BuildContext context, int position) {
          return Card(
            elevation: 2,
            child: Container(
              padding: EdgeInsets.all(10.0),
              child: Row(children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        this.contactList[position].surname +
                            ' ' +
                            this.contactList[position].other_names,
                        style: TextStyle(
                            fontSize: 17, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        this.contactList[position].location,
                        style: TextStyle(
                            fontSize: 15, fontStyle: FontStyle.italic),
                      ),
                      contactList[position].status == 0
                          ? Text(
                              "Pending",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontStyle: FontStyle.italic,
                                  color: Colors.red),
                            )
                          : Text(
                              "Synced",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontStyle: FontStyle.italic,
                                  color: Colors.green),
                            ),
//                      Text(
//                        this.contactList[position].phone_number,
//                        style: TextStyle(
//                            fontSize: 15, fontStyle: FontStyle.italic),
//                      )
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: <Widget>[
                          IconButton(
                              icon: Icon(Icons.remove_red_eye),
                              onPressed: () {
                                _showContactDialog(
                                    context, contactList[position]);
                              }),
                          IconButton(
                              icon: Icon(Icons.edit),
                              onPressed: () {
                                navigateToDetail(contactList[position], 'Edit');
                              }),
                          IconButton(
                              icon: Icon(Icons.delete),
                              onPressed: () {
                                _showAlertDialog(context, contactList[position],
                                    'Are you sure?');
                              })
                        ],
                      ),
                    )
                  ],
                ),
              ]),
            ),
          );
        });
  }

  void navigateToDetail(Contact contact, String appBarTitle) async {
    bool result =
        await Navigator.push(context, MaterialPageRoute(builder: (context) {
      return ContactDetail(contact, appBarTitle);
    }));

    if (result == true) {
      updateListView();
    }
  }

  void _showAlertDialog(BuildContext context, Contact contact, String message) {
    AlertDialog alertDialog = AlertDialog(
      title: Text('', style: TextStyle(fontSize: 10)),
      content: Text(
        message,
        style: TextStyle(fontSize: 15),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('Yes'),
          onPressed: () {
            Navigator.pop(context);
            _delete(context, contact);
            debugPrint('yes');
          },
        ),
        FlatButton(
            child: Text('No'),
            onPressed: () {
              Navigator.pop(context);
              debugPrint('no');
            })
      ],
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }

  void _showContactDialog(BuildContext context, Contact contact) {
    AlertDialog alertDialog = AlertDialog(
      title: Text(contact.surname + ' ' + contact.other_names,
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
      content: Text(
        '${contact.location}\n${contact.phone_number}',
        style: TextStyle(fontSize: 20),
      ),
      actions: <Widget>[
        FlatButton(
            child: Text('Ok'),
            onPressed: () {
              Navigator.pop(context);
            })
      ],
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }

  void _delete(BuildContext context, Contact contact) async {
    int result = await _dbHelper.deleteContact(contact.id);

    if (result != 0) {
      _showSnackBar(context, 'Contact deleted successfully');
      updateListView();
    }
  }

  void _showSnackBar(BuildContext context, String message) {
    final snackbar = SnackBar(
      content: Text(message),
      backgroundColor: Colors.red,
    );
    Scaffold.of(context).showSnackBar(snackbar);
  }

  void updateListView() {
    final Future<Database> dbFuture = _dbHelper.initDB();
    dbFuture.then((database) {
      Future<List<Contact>> contactListFuture = _dbHelper.getContactList();
      contactListFuture.then((contactList) {
        setState(() {
          this.contactList = contactList;
          this.count = contactList.length;
        });

        print("List of things" + json.encode(contactList));
      });
    });
  }

  void doSyncAction() {
    final Future<Database> dbFuture = _dbHelper.initDB();
    dbFuture.then((database) {
      Future<List<Contact>> contactListFuture = _dbHelper.getUnsyncedContact();

      print("Future Contact List " + json.encode(contactListFuture.toString()));

      contactListFuture.then((contactList) {
        setState(() {
          createPost(contactList);
        });
        var jsonThinf = json.encode(contactList);
        // List<dynamic> map = jsonDecode(jsonThinf);
        print("JSON  List " + jsonThinf);
        // print("Surname List " + contactList.);
        print("Contact List " + json.encode(contactList));
      });
    });
  }

  void createPost(List<Contact> contact) async {
    var count = contact.length;

    var map = new Map<String, dynamic>();
    map["officers"] = contact;

    // if (contact != null) {
    try {
      final response = await http.post(OFFICERS_ENDPOINT,
          headers: {
            HttpHeaders.contentTypeHeader: 'application/json',
          },
          body: json.encode(map) //postToJson(paymentModel)
          );

      print('***************${response.request.toString()}');
      print('***************${response.statusCode.toString()}');
      print('***************${response.body.toString()}');
      print('***************${json.encode(contact)}');

      if (response.statusCode == 200) {
        for (int i = 0; i < count; i++) {
          int result = await _dbHelper.updateUnsyncedContact(contact[i].uuid);

          if (result != 0) {
            //_showSnackBar(context, 'Contact deleted successfully');
            updateListView();
          }
          print('***************Updated $result*********************');
          print('***************UUID ${contact[i].uuid.toString()}');
        }
      } else {
        Fluttertoast.showToast(
            msg: 'Sorry something went wrong. Please try again later.',
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIos: 2,
            backgroundColor: Colors.red,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    } catch (e) {
      print(e);
    }
    //}

//    final result = json.decode(response.body);
//
//    // if(response.statusCode == 200) {
//    if (result['resp_code'] == '000') {
//      _showDialog(result['resp_desc']);
//    } else {
//      _showDialog((result['resp_desc']));
//    }
//
//    print('Body ${response.body}');

    // return response;
  }
}
